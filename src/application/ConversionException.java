package application;

import javafx.scene.control.Label;

public class ConversionException extends Exception {

	String label;
	

	public ConversionException(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
}
	

