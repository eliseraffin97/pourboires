package application;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class VueController {

	@FXML Button calculate;
	@FXML TextField bill;
	@FXML TextField tip;
	@FXML TextField people;
	@FXML Label result;
	@FXML Label total;
	@FXML Label erreur;
	@FXML DatePicker date;

	public void clickMe(ActionEvent a) {

		try {

			this.erreur.setText("");

			float billInput = convertir(this.bill);
			float tipInput = convertir(this.tip);
			float nbPeople = convertir(this.people);

			convertirDate();

			// calcul le pourboire par personne
			float tipPerPerson = ((tipInput/100)*billInput)/nbPeople;

			// calcul le total par personne, pourboire inclus
			float totalPerPerson = (billInput/nbPeople)+tipPerPerson;

			result.setText(Float.toString(tipPerPerson));
			total.setText(Float.toString(totalPerPerson));

			traitement_historique(billInput, tipInput, nbPeople);


		} catch (ConversionException e) {
			// TODO Auto-generated catch block
			this.erreur.setText(e.getLabel() + "Merci de modifier votre saisie.");
		}


	}

	public void changeDate(ActionEvent a) {

		try {
			String dateString = (date.getValue()).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			File fileHisto = new File("historique.txt");
			Map<String, Map<String, Float>> donnees = new HashMap<>();
			
			if (fileHisto.exists()) {
				ObjectInputStream flux = new ObjectInputStream(new FileInputStream(fileHisto));
				donnees = (Map<String, Map<String, Float>>) flux.readObject();
				if (donnees.containsKey(dateString)) {
					this.bill.setText(Float.toString(donnees.get(dateString).get("bill")));
					this.tip.setText(Float.toString(donnees.get(dateString).get("tip")));
					this.people.setText(Float.toString(donnees.get(dateString).get("nbPeople")));
				}
			}
		} catch (IOException e) {
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}


	private void traitement_historique(float billInput, float tipInput, float nbPeople) {

		try {

			String dateString = (date.getValue()).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));


			File fileHisto = new File("historique.txt");
			Map<String, Map<String, Float>> donnees = new HashMap<>();

			if (!fileHisto.exists()) {
				fileHisto.createNewFile();
			}

			ObjectInputStream flux = new ObjectInputStream(new FileInputStream(fileHisto));

			donnees = (Map<String, Map<String, Float>>) flux.readObject();

			Map chiffres = new HashMap<String, Float>();
			chiffres.put("bill", billInput);
			chiffres.put("tip", tipInput);
			chiffres.put("nbPeople", nbPeople);

			donnees.put(dateString, chiffres);

			flux.close();


			try {
				ObjectOutputStream fluxOutput = new ObjectOutputStream(new FileOutputStream(fileHisto));

				fluxOutput.writeObject(donnees);
				fluxOutput.close();
				
			} catch (IOException ioe) {
				System.err.println(ioe);
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}


	private float convertir(TextField textField) throws ConversionException {
		try {
			float result = Integer.parseInt(textField.getText());
			return result;
		} catch (NumberFormatException e) {
			throw new ConversionException(textField.getId() + " doit �tre un nombre. ");
		}
	}


	private void convertirDate() throws ConversionException {
		try {
			LocalDate localDate = this.date.getValue();
			Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Date date = Date.from(instant);
		} catch (NullPointerException e) {
			throw new ConversionException("La date doit �tre dans le format suivant: 'dd-mm-yyyy'. ");
		}
	}

}
